package listas;

import arboles.Arbol;
import java.util.ArrayList;
import java.util.List;
import objetosNegocio.Cliente;
import static pruebas.ProyectoFinalEstructura.contC;

public class Lista {
    
    List listClient;
    
    
        public  int cont;

    public  int getCont() {
        return cont;
    }
        
        

   public NodoLista cabecera;
      

    //CONSTRUCTOR
    public Lista(){
        this.cabecera=new NodoLista(null);
    }

    //GET Y SET
    public NodoLista getCabecera() {
        return cabecera;
    }
    public void setCabecera(NodoLista cabecera) {
        this.cabecera = cabecera;
    }
    
    //OPERACIONES
    public boolean estaVacia(){
        if(this.cabecera.getSiguiente()==null){
            return true;
        } 
        else{
            return false;
        }
    }
    public IteradorLista buscar(Object o){
        NodoLista nodo=this.cabecera.getSiguiente();
        while (nodo!=null && !nodo.getElemento().equals(o)){
        nodo=nodo.getSiguiente();
        }
        return new IteradorLista(nodo);
    }
    public void insertarCliente(Object o, IteradorLista it){
        
        if (it!=null && it.getActual()!=null){
            it.getActual().setSiguiente(new NodoLista(o,it.getActual().getSiguiente()));
        }
    }
    
    public void eliminarCliente(Object o){
        IteradorLista it=encontrarPrevio(o);
        if (it.getActual().getSiguiente()!=null){
            it.getActual().setSiguiente(it.getActual().getSiguiente().getSiguiente());
          
        }
    }
    public IteradorLista encontrarPrevio(Object o){
        NodoLista nodo=this.cabecera;
        while (nodo.getSiguiente()!=null && !nodo.getSiguiente().getElemento().equals(o)){
            nodo=nodo.getSiguiente();
        }
        return new IteradorLista(nodo);
    }
    public void listarClientes(){
        NodoLista nodo=this.cabecera.getSiguiente();
        while(nodo!=null){
            Cliente p=(Cliente)nodo.getElemento();
            System.out.println(p);
            nodo=nodo.getSiguiente();
        }
    }
    
    
     public List <Cliente> agregarJera(){
          List <Cliente> listClient = new ArrayList();
 
      
        NodoLista nodo=this.cabecera.getSiguiente();
        if (nodo==null) {
        }
        while(nodo!=null){
            Cliente c=(Cliente)nodo.getElemento();
            
            listClient.add(c);
            nodo=nodo.getSiguiente();
        }
         
       return listClient;  
    }
     
     
     
    public void despliegaListaClientes(){
 
      
        NodoLista nodo=this.cabecera.getSiguiente();
        if (nodo==null) {
            System.out.println("No hay clientes");
        }
        while(nodo!=null){
            Cliente c=(Cliente)nodo.getElemento();
            System.out.println(c);
            nodo=nodo.getSiguiente();
        }
    }
    
    public Cliente buscarClienteRangoSaldos(double desde,double hasta)
    {
          cont = 0;

        Cliente vac= new Cliente();
        NodoLista nodo=this.cabecera.getSiguiente();
          while(nodo!=null){
         while(nodo!=null){
       
            Cliente c=(Cliente)nodo.getElemento();
            if (c.getSaldo() >= desde && c.getSaldo() <= hasta)
            {
         System.out.println(c);
                
      cont++;
            }
            nodo=nodo.getSiguiente();  
           
        }   
         
        
          
           
   
         

      }
        return null;
    }
    
    
    public Cliente buscarID(int id){
        cont =0;
        NodoLista nodo=this.cabecera.getSiguiente();
        while(nodo!=null){
            Cliente c=(Cliente)nodo.getElemento();
            //if(c.getId().equals(id))
            if(id==c.getId())
            {cont++;
                return c;
            } 
            nodo=nodo.getSiguiente();
                 
        } return null;
    }
    
    public Cliente buscarNombre(String nombre) {
        cont =0;
        NodoLista nodo = this.cabecera.getSiguiente();
        while (nodo != null) {
            Cliente p = (Cliente) nodo.getElemento();
            if (p.getNombre().equals(nombre)) {
                System.out.println(p);
                 cont++;
            }
            nodo = nodo.getSiguiente();
           
        }
        
        return null;
    }

    public Cliente buscarApellido(String apellido) {
            cont = 0;
        NodoLista nodo = this.cabecera.getSiguiente();
       
        while (nodo != null) {
            Cliente p = (Cliente) nodo.getElemento();
            if (p.getApellido().equals(apellido)) {
                System.out.println(p);
                cont++;
            }
            nodo = nodo.getSiguiente();
               
        }
        
        
        return null;
    }
    
    public void actualizarNombre(String nuevoNombre,Cliente cliente){
        cliente.setNombre(nuevoNombre);
        System.out.println("Nuevo nombre: ");
    }
    
    public void actualizarApellido(String nuevoApellido,Cliente cliente){
        cliente.setApellido(nuevoApellido);
        System.out.println("Nuevo apellido: ");
    }
    
    public void actualizarID(int nuevoID,Cliente cliente){
        cliente.setId(nuevoID);
        System.out.println("Nueva ID: ");
    }
    
    public void actualizarEdad(int nuevaEdad,Cliente cliente){
        cliente.setEdad(nuevaEdad);
        System.out.println("Nueva edad: ");
    }
    
    public void actualizarSaldo(double saldo,Cliente cliente){
        cliente.setSaldo(saldo);
        System.out.println("Nuevo saldo: ");
    }

    public void imprimirRango() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}