package listas;
public class IteradorLista {
    private NodoLista actual;

    //CONSTRUCTOR
    public IteradorLista(NodoLista n){
        this.actual=n;
    }

    //GET Y SET
    public NodoLista getActual() {
        return actual;
    }
    public void setActual(NodoLista actual) {
        this.actual = actual;
    }

    //OPERACIONES
    public boolean estaFuera(){
        if (actual==null)
            return true;
        else
            return false;
    }
    public Object obtener(){
        return this.actual.getElemento();
    }
    public  void avanzar(){
        if(!estaFuera()){
            this.actual=this.actual.getSiguiente();
        }
    }
}