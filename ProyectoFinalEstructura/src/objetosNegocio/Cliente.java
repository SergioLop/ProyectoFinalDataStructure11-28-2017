/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package objetosNegocio;

import arboles.Nodo;

/**
 *
 * @author eljai_000
 */
public class Cliente {
    
    private int id, edad;
    private double saldo;
    private String nombre, apellido;
    
    public Cliente() {
        
    }

    public Cliente(int id, int edad, double saldo, String nombre, String apellido) {
        this.id = id;
        this.edad = edad;
        this.saldo = saldo;
        this.nombre = nombre;
        this.apellido = apellido;
    }

    

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public double getSaldo() {
        return saldo;
    }

    public void setSaldo(double saldo) {
        this.saldo = saldo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }
 
    @Override
    public String toString()
    {
        return "ID cliente: " + id + ", Edad: " + edad +
                ", Nombre: " + nombre + ", Apellido: " + apellido +
                ", Sueldo: " + saldo;
    }
    
    public String toString2()
    {
        return id + nombre + apellido + saldo;
    }
}
