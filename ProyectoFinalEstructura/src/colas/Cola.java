/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package colas;

import objetosNegocio.Turno;
import pruebas.ProyectoFinalEstructura;
import static pruebas.ProyectoFinalEstructura.contC;

/**
 *
 * @author eljai_000
 */
public class Cola {

    public NodoCola cabecera;
 
    public Cola() {
        cabecera = new NodoCola(null);
    }

    public void setCabecera(NodoCola cabecera) {
        this.cabecera = cabecera;
    }

    public NodoCola getCabecera() {
        return cabecera;
    }

    public boolean estaVacia() {
        if (cabecera.getElemento() == null && cabecera.getSiguiente() == null) {
            return true;
        }
        return false;
    }
   public int contCc =0;
    public IteradorCola buscar(int num) {
        NodoCola nodo = this.cabecera.getSiguiente();
        while (nodo != null) {
            Turno t = (Turno) nodo.getElemento();
            if (t.getNumero() == num) {
                IteradorCola nuevoIt = new IteradorCola(nodo);
                return nuevoIt;
            }
            nodo = nodo.getSiguiente();
        }
        return null;
    }

    public void insertar(IteradorCola previo, Turno nuevo) {
        if (previo != null && nuevo != null) {
            previo.getActual().setSiguiente(new NodoCola(nuevo, previo.getActual().getSiguiente()));
        }
    }

    public void eliminar(Object o) {
        IteradorCola it = encontrarPrevio(o);
        if (it.getActual().getSiguiente() != null) {
            it.getActual().setSiguiente(it.getActual().getSiguiente().getSiguiente());
        }
        
        
    }

    public IteradorCola encontrarPrevio(Object o) {
        
        NodoCola nodo = this.cabecera;
        while (nodo.getSiguiente() != null && !nodo.getSiguiente().getElemento().equals(o)) {
            nodo = nodo.getSiguiente();
        }
        return new IteradorCola(nodo);
    }

    public Turno despliegaCola() {
        


       NodoCola nodo = this.cabecera.getSiguiente();
           if (contC == 0) {
            System.out.println("Sin turnos que atender");
            return null;
        }
   
           else{ while (nodo != null) {
            Turno t = (Turno) nodo.getElemento();
            System.out.println("Turno numero: " + t.getNumero() + ", Tramite: " + t.getTramite() + "\n");
            nodo = nodo.getSiguiente();
      
            
        }
        
           }
      
        
        return null;
    }

    
}
