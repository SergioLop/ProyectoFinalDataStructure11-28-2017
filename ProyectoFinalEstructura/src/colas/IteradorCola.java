package colas;

import objetosNegocio.Turno;

public class IteradorCola {

    private NodoCola actual;

    public IteradorCola(NodoCola actual) {
        this.actual = actual;
    }

    public NodoCola getActual() {
        return actual;
    }

    public void setActual(NodoCola actual) {
        this.actual = actual;
    }

    public boolean estaFuera() {
        if (actual == null) {
            return true;
        }
        return false;
    }

    public Turno obtener() {
        return (Turno) actual.getElemento();
    }

    public void avanzar() {
        if (estaFuera() == false) {
            actual = actual.getSiguiente();
        }
    }
}
