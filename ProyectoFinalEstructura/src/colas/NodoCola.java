package colas;

import listas.*;
import objetosNegocio.Turno;

public class NodoCola {
    private Object elemento;
    private NodoCola siguiente;

    //CONSTRUCTORES
    public NodoCola(Turno elemento){
        this.elemento=elemento;
        this.siguiente=null;
    }
    public NodoCola(Turno elemento,NodoCola siguiente){
        this.elemento=elemento;
        this.siguiente=siguiente;
    }

    //GET'S Y SET'S
    public Object getElemento() {
        return elemento;
    }
    public void setElemento(Object elemento) {
        this.elemento = elemento;
    }
    public NodoCola getSiguiente() {
        return siguiente;
    }
    public void setSiguiente(NodoCola siguiente) {
        this.siguiente = siguiente;
    }
}