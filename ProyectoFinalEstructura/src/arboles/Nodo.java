/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package arboles;

/**
 *
 * @author eljai_000
 */
public class Nodo {

    private Nodo pd;
    private Object dato;
    private Nodo pi;

    public Nodo(Object dato) {
        this.dato = dato;
        pd = null;
        pi = null;
    }

    public Object getDato() {
        return dato;
    }

    public void setDato(Object dato) {
        this.dato = dato;
    }

    public Nodo getPd() {
        return pd;
    }

    public void setPd(Nodo pd) {
        this.pd = pd;
    }

    public Nodo getPi() {
        return pi;
    }

    public void setPi(Nodo pi) {
        this.pi = pi;
    }

}
