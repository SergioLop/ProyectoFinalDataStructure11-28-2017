/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package arboles;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import listas.Lista;
import objetosNegocio.Cliente;

/**
 *
 * @author eljai_000
 */
public class Arbol {

    List<Cliente> listaClientess;

    public Arbol(List<Cliente> listaClientess) {
        this.listaClientess = listaClientess;
    }

    public List<Cliente> getListaClientess() {
        return listaClientess;
    }

    private Nodo Raiz;
    Cliente cl = new Cliente();

    public Arbol() {
        Raiz = null;

    }

    public boolean arbolVacio() {
        return Raiz == null;
    }

    public void insertar(Nodo ant, Nodo p, Nodo x) {
        if (p != null) {
            if (x.getDato().toString().equals(p.getDato().toString())) {
                insertar(p, p.getPi(), x);
            } else {
                insertar(p, p.getPd(), x);
            }
        } else if (arbolVacio()) {
            Raiz = x;
        } else //if (Integer.parseInt(x.getDato().toString()) > Integer.parseInt(ant.getDato().toString())) 
        {
            if (x.getDato().toString().equals(ant.getDato().toString())) {
                ant.setPd(x);
            } else {
                ant.setPi(x);
            }
        }
    }

    public void imprimir(List<Cliente> Clienteslist) {
        int c = 0;

        ArrayList<Cliente> listaPlus = new ArrayList();
        ArrayList<Cliente> listaOro = new ArrayList();
        ArrayList<Cliente> listaPlatino = new ArrayList();
        ArrayList<Cliente> listaDiamante = new ArrayList();

        for (Cliente Clientes : Clienteslist) {

            if (Clientes.getSaldo() >= 0.0 && Clientes.getSaldo() <= 24999.99) {
                listaPlus.add(Clientes);
                c++;
            }
            //
            if (Clientes.getSaldo() >= 25000 && Clientes.getSaldo() <= 14999.99) {
                listaOro.add(Clientes);

            }
            //
            if (Clientes.getSaldo() >= 150000 && Clientes.getSaldo() <= 499999.99) {

                listaPlatino.add(Clientes);
            }
            //
            if (Clientes.getSaldo() >= 500000) {

                listaDiamante.add(Clientes);
            }
        }
        System.out.println("Cliente PLUS");
        for (Cliente cliente : listaPlus) {

            System.out.println(cliente);

        }
        if (listaPlus.isEmpty()) {
            System.out.println("404 NO CLIENTS FOUND");

        }

        System.out.println("");
        System.out.println("Clientes ORO");
        for (Cliente cliente : listaOro) {

            System.out.println(cliente);
        }

        if (listaOro.isEmpty()) {
            System.out.println("404 NO CLIENTS FOUND");

        }
        System.out.println("");
        System.out.println("Clientes PLUS");
        for (Cliente cliente : listaPlatino) {

            System.out.println(cliente);
        }

        if (listaPlatino.isEmpty()) {
            System.out.println("404 NO CLIENTS FOUND");

        }
        System.out.println("");
        System.out.println("Clientes DIAMANTE");
        for (Cliente cliente : listaDiamante) {

            System.out.println(cliente);
        }
        if (listaDiamante.isEmpty()) {
            System.out.println("404 NO CLIENTS FOUND");

        }

    }

    public Nodo getRaiz() {
        return Raiz;
    }

    public void setRaiz(Nodo raiz) {
        Raiz = raiz;
    }

    public void eliminar(Nodo ant, Nodo p) {
        if (esNodoHoja(p)) {
            eliminarNodoHoja(ant, p);
        } else if (esNodoCon2SubArboles(p)) {
            eliminar(ant, p);
        } else {
            eliminarNodoCon1SubArnol(ant, p);
        }
    }

    public void eliminarNodoHoja(Nodo ant, Nodo p) {
        if (Raiz != p) {
            if (ant.getPd() == p) {
                ant.setPd(null);
            } else {
                ant.setPi(null);
            }
        } else {
            Raiz = null;
        }
    }

    public void eliminarNodoCon1SubArnol(Nodo ant, Nodo p) {
        if (Raiz == p) {
            if (p.getPd() != null) {
                Raiz = Raiz.getPd();
            } else {
                Raiz = Raiz.getPi();
            }
        } else if (ant.getPd() == p) {
            if (p.getPd() != null) {
                ant.setPd(p.getPd());
            } else {
                ant.setPd(p.getPi());
            }
        } else if (p.getPd() != null) {
            ant.setPi(p.getPd());
        } else {
            ant.setPi(p.getPi());
        }
    }

    public boolean esNodoHoja(Nodo p) {

        return (p.getPi() == null && p.getPd() == null);

    }

    public boolean esNodoCon2SubArboles(Nodo p) {

        return (p.getPi() != null && p.getPd() != null);

    }

}
