/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pruebas;

import arboles.*;

import java.util.Scanner;

import listas.*;

import colas.*;

import java.util.ArrayList;

import java.util.List;

import objetosNegocio.*;

/**
 *
 * @author eljai_000
 */
public class ProyectoFinalEstructura {
public static int contC = 0;

    public static int getContC() {
        return contC;
    }
    

    /**
     * @param args the command line arguments
     */
    
    static Scanner tec = new Scanner(System.in);

    public static void main(String[] args) {

        
       /**
        * Una nave xd
        */
    Cola cola = new Cola();
                  Lista lista = new Lista();
                       Arbol miArbol = new Arbol();
                    IteradorLista iterador = new IteradorLista(lista.getCabecera());
                         IteradorCola iteradorC = new IteradorCola(cola.getCabecera());
                               int idCliente = 0, edadCliente = 0, menu1 = 0, menu2 = 0,
                               actualizar = 0, actualizar2 = 0, clienteDel = 0;
              String nombreCliente = null, apellidoCliente = null;
        double saldoCliente = 0;

        
        
        while (menu1 != 4) {
            System.out.println("--------------- B A N C O ---------------");
            System.out.println("Bienvenido a nuestro sistema de banco, escoga la opción que desee: ");
            System.out.println("1.- Administración de clientes. ");
            System.out.println("2.- Simulación de turnos. ");
            System.out.println("3.- Jerarquía de clientes.  ");
            System.out.println("4.- Salir. ");
            menu1 = tec.nextInt();

            switch (menu1) {
                case 1:
                    System.out.println("--------------- ADMIN. DE CLIENTES ---------------");
                    System.out.println("Que desea realizar?: ");
                    System.out.println("1.- Agregar cliente.");
                    /* */
                    System.out.println("2.- Actualizar cliente.");
                    /* */
                    System.out.println("3.- Eliminar cliente.");
                    System.out.println("4.- Buscar cliente por nombre.");
                    System.out.println("5.- Buscar cliente por apellido.");
                    System.out.println("6.- Buscar cliente por rango de salgos (desde,hasta).");
                    System.out.println("7.- Mostrar lista de clientes. ");
                    menu2 = tec.nextInt();
                    //break;
                    if (menu2 == 1) { //AGREGAR
                        System.out.println("Introduzca el nombre del nuevo cliente: ");
                        nombreCliente = tec.next();
                        System.out.println("Introduzca el apellido del nuevo cliente: ");
                        apellidoCliente = tec.next();
                        System.out.println("Introduzca el ID del nuevo cliente: ");
                        idCliente = tec.nextInt();
                        System.out.println("Introduzca la edad del nuevo cliente: ");
                        edadCliente = tec.nextInt();
                        System.out.println("Introduzca el saldo del nuevo cliente: ");
                        saldoCliente = tec.nextDouble();

                        Cliente nuevoCliente = new Cliente(idCliente, edadCliente, saldoCliente, nombreCliente, apellidoCliente);
                        lista.insertarCliente(nuevoCliente, iterador);

                        //ARBOL
                        Nodo x = new Nodo(nuevoCliente);
                        miArbol.insertar(miArbol.getRaiz(), miArbol.getRaiz(), x);

                        System.out.println("Cliente agregado con éxito.");
                        tec.next();
                    } else if (menu2 == 2) { //ACTUALIZAR
                        System.out.println("Ingrese la ID del cliente a actualizar: ");
                        actualizar2 = tec.nextInt();
                        Cliente clienteAct = lista.buscarID(actualizar2);

                        if (clienteAct == null) {
                            System.out.println("Cliente no encontrado.");
                        } else {
                            System.out.println("¿Que desea actualizar del cliente? ");
                            System.out.println("1.- Nombre(s) del cliente.");
                            System.out.println("2.- Apellidos del cliente.");
                            System.out.println("3.- ID del cliente.");
                            System.out.println("4.- Edad del cliente.");
                            System.out.println("5.- Saldo del cliente.");
                            actualizar = tec.nextInt();

                            if (actualizar == 1) {
                                System.out.println("Indique el nuevo nombre del cliente: ");
                                String nuevoNombre = tec.nextLine();
                                lista.actualizarNombre(nuevoNombre, clienteAct);
                                System.out.println("Cliente actualizado con exito.");
                                tec.next();
                            } else if (actualizar == 2) {
                                System.out.println("Indique el nuevo nombre del cliente: ");
                                String nuevoApellidos = tec.nextLine();
                                lista.actualizarApellido(nuevoApellidos, clienteAct);
                                System.out.println("Cliente actualizado con exito.");
                                tec.next();
                            } else if (actualizar == 3) {
                                System.out.println("Indique el nuevo ID del cliente: ");
                                int nuevaID = tec.nextInt();
                                lista.actualizarID(nuevaID, clienteAct);
                                System.out.println("Cliente actualizado con exito.");
                                tec.next();
                            } else if (actualizar == 4) {
                                System.out.println("Indique la edad del cliente: ");
                                int nuevaEdad = tec.nextInt();
                                lista.actualizarEdad(nuevaEdad, clienteAct);
                                System.out.println("Cliente actualizado con exito.");
                                tec.next();
                            } else if (actualizar == 5) {
                                System.out.println("Indique el nuevo saldo del cliente: ");
                                float nuevoSaldo = tec.nextInt();
                                lista.actualizarSaldo(nuevoSaldo, clienteAct);
                                System.out.println("Cliente actualizado con exito.");
                                tec.next();
                            } else {
                                System.out.println("Indique una opción valida.");
                            }
                        }
                    } else if (menu2 == 3) //ELIMINAR 
                    {
                        System.out.println("Introduzca la ID del cliente a eliminar: ");
                        clienteDel = tec.nextInt();

                        Cliente clienteDelete = lista.buscarID(clienteDel);

                        if (clienteDelete != null) {
                            lista.eliminarCliente(clienteDelete);
                            System.out.println("Cliente eliminado.");
                            tec.next();
                            
                            
                        }   if(lista.getCont()==0){
                          System.out.println("Clientes no encontrados");}
                        
                    } else if (menu2 == 4) //BUSCAR NOMBRE
                    {
                        String nombreBusqueda = null;
                        System.out.println("Introduzca el nombre del cliente a buscar: ");
                        nombreBusqueda = tec.next();
                        Cliente nombreBuscar = lista.buscarNombre(nombreBusqueda);

                        if (nombreBuscar != null) {
                            System.out.println("Datos del cliente: " + nombreBuscar);
                            tec.next();
                            
                        }    if(lista.getCont()==0){
                          System.out.println("Clientes no encontrados");
                        }
                    } else if (menu2 == 5) //BUSCAR APELLIDO
                    {
                        String apellidoBusqueda = null;
                        System.out.println("Introduzca el apellido del cliente a buscar: ");
                        apellidoBusqueda = tec.next();
                        Cliente apellidoBuscar = lista.buscarApellido(apellidoBusqueda);

                        if (apellidoBuscar != null) {
                            System.out.println("Datos del cliente: " + apellidoBuscar);
                            tec.next();
                            
                            
                        }   
                        if(lista.getCont()==0){
                          System.out.println("Clientes no encontrados");}
                        
                    } else if (menu2 == 6) // RANGOS 
                    {
                        double desde = 0.0f, hasta = 0.0f;
                        System.out.println("Escriba desde donde quiere buscar un saldo: ");
                        desde = tec.nextInt();

                        System.out.println("Escriba hasta donde quiere buscar un saldo: ");
                        hasta = tec.nextInt();

                     Cliente rangoSaldos = lista.buscarClienteRangoSaldos(desde, hasta);
                        if (rangoSaldos != null) {
                            System.out.println("Clientes dentro del rango ");
                            System.out.println(rangoSaldos);
                            tec.next();

                        }   
                      if(lista.getCont()==0){
                          System.out.println("Clientes no encontrados");}
//                        else { System.out.println("Clientes no encontrados");}

                    } else if (menu2 == 7) //LISTA CLIENTES
                    {
                        lista.despliegaListaClientes();
                        tec.next();
                    }
                    break;
                case 2:

                    System.out.println("--------------- SIMULACION DE TURNOS ---------------");
                  
                    int turno = 0;
                    
                    System.out.println("Escoja la opcion que desee: ");
                    System.out.println("1.- Otorgar turno. ");
                    System.out.println("2.- Consultar turnos pendientes. ");
                    System.out.println("3.- Atender turnos. ");
                    System.out.println("4.- Consultar ultimo turno atendido. ");
                    turno = tec.nextInt();

                    if (turno == 1) {
                        System.out.println("Que tipo de tramite desea realizar? :");
                        System.out.println("Cajas, Ejecutivo, Afore. ");
                        otorgarTurno(cola, iteradorC);
                    } else if (turno == 2) {
                        System.out.println("Turnos pendientes: ");
                        cola.despliegaCola();
                        tec.next();
                    } else if (turno == 3) {
                        System.out.println("Atender cliente: ");
                        atenderTurno(cola);
                    } else if (turno == 4) {
                        
                        try{
                        System.out.println("Atendiendo al ultimo cliente: ");
                        System.out.println("Numero: " + Turno.getUltimoAtendido().getNumero()
                                + ", Tramite: " + Turno.getUltimoAtendido().getTramite());}
                        catch (NullPointerException e){
                            System.out.println("No se han otorgado turnos");}
                    }
                    break;

                case 3:
                     List<Cliente> clientes = new ArrayList(lista.agregarJera());
                     Arbol listaClientesJera = new Arbol();
                    /*
                    System.out.printf("%40s", "____________ \n");
                    System.out.printf("%40s", "| Clientes | \n");
                    System.out.printf("%40s", "____________ \n");
                    System.out.printf("%37s", "   ||   \n");
                    System.out.printf("__________________________________________________________________________ \n");
                    System.out.printf("|                       |                         |                      | \n");
                    System.out.printf("________              _______                    ___                  ____ \n");
                    System.out.printf("Diamante              Platino                    Oro                  Plus \n");
                    System.out.printf("________              _______                    ___                  ____ \n");
                    System.out.printf("|                        |                        |                      | \n");
                     */
                    System.out.println("========= J E R A R Q U I A ============");
                   listaClientesJera.imprimir(clientes);
                    System.out.println("");
                    System.out.println("========================================");
                    tec.next();
                    
            }
            
        }
        
        
        
    }

    public static Cliente otorgarTurno(Cola cola, IteradorCola iteradorC) {
        String proceso = tec.next();

        if (proceso.equalsIgnoreCase("cajas")
                || proceso.equalsIgnoreCase("ejecutivo")
                || proceso.equalsIgnoreCase("afore")) {
            Turno turno = new Turno(proceso);
            System.out.println("Turno numero: " + turno.getNumero() + ", Tramite: " + turno.getTramite());
            cola.insertar(iteradorC, turno);
            iteradorC.setActual(iteradorC.getActual().getSiguiente()); 
            
            contC++;
        } else {
            System.out.println("Turno no validado");
        }
        
        return null;
    }

    public static Cliente atenderTurno(Cola cola) {
        try {
            if (Turno.getUltimoAtendido() == null) {
                Turno.setUltimoAtendido((Turno) cola.getCabecera().getSiguiente().getElemento());
                
                //(Turno.setUltimoAtendido() != null);
             contC--;   System.out.println("Numero de turno recien atendido: " + Turno.getUltimoAtendido().getNumero());
            } else {
                Turno.setUltimoAtendido((Turno) cola.buscar(Turno.getUltimoAtendido().getNumero()).getActual().getSiguiente().getElemento());
              
                System.out.println("Recien se atendio al turno numero :   " + Turno.getUltimoAtendido().getNumero());
                 cola.eliminar(cola);
                if (contC<0) {
                    contC=0;
                }
            }
            return null;
        } catch (NullPointerException e) {
            System.out.println("Sin turnos que antender");
           
            
            return null;
        }

    }

}
